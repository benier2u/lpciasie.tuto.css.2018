# Tutoriels LP CIASIE

## module Intégration & Documents Web

Construire un tutoriel basé sur une démo sur un des thèmes listés ci-dessous.
Il faudra également prévoir une courte présentation (10mn/tuto) dans la semaine du 22-25 octobre.
Chaque tutoriel correspond à un des répertoires du projet.
Le tutoriel en lui-même, ou un lien, doit être décrit dans le fichier README.md du répertoire, en utilisant la syntaxe markdown.

## DATE DE RENDU : 28 octobre 2018, 24:00

## thèmes prévus :
1. css transition, multiple column layout: Benier, Lemmer, Van Haaren, Robert
2. css gradients, css color module : Spacher, Borg, Dufour
3. filter effects, blending mode: Felix, Dubouis, Marly, Rohrbacher,
4. css animation : Thouvenin, Ralli, Saltutti, Zink
5. css transform : Galassi, Rimet, Butaye, Schwarz
6. postCss & autoprefixer: Charlot, Jezequel, Wittmann, Bernard
7. framework css, version sass : Briand, Wirtz, Barottin

## Ce qui est attendu :
1. introduction, rappels et pointeur vers la spécification W3C et une référence de qualité et complète
2. le tutoriel expliquant l'intéret et l'utilisation du module css, basé sur une démo
3. la démo elle-même, avec les sources, éventuellement sur codepen
4. un point sur l'implantation dans les navigateurs
5. une liste de ressources/exemples/tutoriels existant sur le même thème


## Utilisation du dépôt

**Ne travaillez pas directement sur ce dépôt**

Faites un fork pour créer un dépôt dont vous êtes propriétaire.

Utilisez ce nouveau dépôt : clonez-le sur votre machine.

**Créez une branche portant le nom de votre tuto !**

Faites vos commits/push sur votre dépôt. Quand votre tutoriel est prêt, faites un pull request pour l'intégrer dans ce dépôt.
